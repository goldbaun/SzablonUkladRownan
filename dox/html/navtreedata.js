var NAVTREE =
[
  [ "Układ Równań Liniowych", "index.html", [
    [ "Program do Rozwiązywania Układów Równań Liniowych", "index.html", [
      [ "Najważniejsze cechy i uwagi", "index.html#etykieta-wazne-cechy", null ]
    ] ],
    [ "README", "md__home_piotr__pulpit__projekty__semestr2_ukladrownanliniowych__r_e_a_d_m_e.html", null ],
    [ "Klasy", "annotated.html", [
      [ "Lista klas", "annotated.html", "annotated_dup" ],
      [ "Indeks klas", "classes.html", null ],
      [ "Składowe klas", "functions.html", [
        [ "Wszystko", "functions.html", null ],
        [ "Funkcje", "functions_func.html", null ],
        [ "Zmienne", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Pliki", null, [
      [ "Lista plików", "files.html", "files" ],
      [ "Składowe plików", "globals.html", [
        [ "Wszystko", "globals.html", null ],
        [ "Funkcje", "globals_func.html", null ],
        [ "Definicje", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_macierz_8cpp.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';