#include "Wektor.hh"

using namespace std;

ostream& operator << (ostream &Strm, const Wektor &Wek )
{
    for (int i = 0; i < ROZMIAR; i++)
    {
        Strm << Wek[i] << "   ";
    }
    Strm << endl;
    return Strm;
}

istream& operator >> (istream &Strm, Wektor &Wek)
{
    for (int i =0; i < ROZMIAR; i++)
    {
        if(Strm.eof()) {
            Strm.setstate(ios_base::failbit);
            break;
        }
        double tmp = 0;
        Strm >> tmp;
        if(Strm.fail()) break;
        else Wek[i] = tmp; 
    }
    return Strm;
}

Wektor operator + (Wektor Wek1, Wektor Wek2)
{
    Wektor Wynik;
    for (int i = 0; i < ROZMIAR; i++)
    {
        Wynik[i] = Wek1[i] + Wek2[i];
    }
    return Wynik;
}

Wektor operator - (Wektor Wek1, Wektor Wek2)
{
    Wektor Wynik;
    for (int i = 0; i < ROZMIAR; i++)
    {
        Wynik[i] = Wek1[i] - Wek2[i];
    }
    return Wynik;
}

Wektor operator * (Wektor Wek1, double Skl)
{
    Wektor Wynik;
    for (int i = 0; i < ROZMIAR; i++)
    {
        Wynik[i] = Wek1[i] * Skl;
    }
    return Wynik;
}

double operator * (Wektor Wek1, Wektor Wek2)
{
    double Wynik = 0;
    for (int i = 0; i < ROZMIAR; i++)
    {
        Wynik += (Wek1[i] * Wek2[i]);
    }
    return Wynik;
}

Wektor operator / (Wektor Wek, double Skl)
{
    Wektor Wynik;
    for (int i = 0; i < ROZMIAR; i++)
    {
        Wynik[i] = Wek[i] / Skl;
    }
    return Wynik;
}

