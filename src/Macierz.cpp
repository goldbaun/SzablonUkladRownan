#include "Macierz.hh"

using namespace std;

Wektor operator * (Macierz Mac, Wektor Wek){
    Wektor Wynik;
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            Wynik[i] = Wynik[i] + (Mac[j][i] * Wek[j]);
        }
        
    }
    return Wynik;
}

ostream& operator << (ostream &Strm, const Macierz &Mac)
{
    for (int i = 0; i < ROZMIAR; i++)
    {
        Strm << Mac[i];
        Strm << endl;
    }
    return Strm;
}

istream& operator >> (istream &Strm, Macierz &Mac)
{
    Wektor tmp;
    for (int i = 0; i < ROZMIAR; i++)
    {
        if(Strm.eof()) {
            Strm.setstate(ios_base::failbit);
            break;
        }
        Strm >> tmp;
        if (Strm.fail()) break;
        else Mac[i] = tmp;
    }
    return Strm;
}

Macierz operator * (Macierz Mac1, Macierz Mac2){
    Macierz Wynik;
    for (int i = 0; i < ROZMIAR; i++)
    {
        for (int j = 0; j < ROZMIAR; j++)
        {
            for (int k = 0; k < ROZMIAR; k++)
            {
                Wynik[i][j] = Wynik[i][j] + (Mac1[i][k] * Mac2[k][j]);
            }
        }
    }
    return Wynik;
}

Macierz operator + (Macierz Mac1, Macierz Mac2){
    Macierz Wynik;
    for (int i = 0; i < ROZMIAR; i++)
    {
        Wynik[i] = Mac1[i] + Mac2[i];
    }
    return Wynik;
}