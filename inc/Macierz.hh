#ifndef MACIERZ_HH
#define MACIERZ_HH
/*!
    \file
    \brief Definicja klasy Macierz

    Plik zawiera definicje klasy Macierz
*/

#include "Wektor.hh"
#include "rozmiar.h"
#include <iostream>

/*!
    \brief Modeluje pojęcie macierzy

    Klasa modeluje pojecie macierzy. Rozmiar macierzy definiuje stała ROZMIAR.
    Każdy wiersz macierzy jest Wektorem.
*/
class Macierz {
  public:

  /*!
    \brief operator []

    Zwraca wektor w podanej kolumnie.

    \param[in] index - indeks kolumny, ktora ma byc zwrocona

    \return Zwraca wektor z podanej kolumny
  */
  Wektor operator [] (unsigned int index) const{
    return Kolumna[index];
  }

  /*!
    \brief operator []

    Wstawia wektor do podanej kolumny.

    \param[in] index - indeks kolumny do ktorej ma byc wpisany wektor

    \return Zwraca adres do podanej kolumny
  */
  Wektor& operator[] (unsigned int index){
    return Kolumna[index];
  }
  
  private:
  /*!
    \brief Wektory w kolumnach Macierzy

    Tablica zawierajaca wektory w poszczegolnych kolumnach macierzy.
  */
  Wektor   Kolumna[ROZMIAR];
};

/*!
    \brief operator *

    Przeciazenie to sluzy do mnozenia macierzy przez wektor.

    \param[in] Mac - Pierwszy skladnik mnozenia (Macierz)
    \param[in] Wek - Drugi skladnik mnozenia (Wektor)

    \return Wektor bedacy wynik mnozenia
*/
Wektor operator * (Macierz Mac, Wektor Wek);

/*!
    \brief operator >>

    Przeciazenie to sluzy do wprowadzenia macierzy.

    \param[in] Strm - Adres do strumienia wejsciowego
    \param[in] Mac - Referencja do Macierzy, w ktorej maja byc zapisane dane ze strumienia wejsciowego

    \return Adres do strumienia wejsciowego
  */
std::istream& operator >> (std::istream &Strm, Macierz &Mac);

/*!
    \brief operator <<

    Przeciazenie to sluzy do wypisania macierzy.

    \param[in] Strm - Adres do strumienia wyjsciowego
    \param[in] Mac - Referencja do Macierzy ktora ma byc wypisana

    \return Adres do strumienia wyjsciowego
*/
std::ostream& operator << (std::ostream &Strm, const Macierz &Mac);

Macierz operator * (Macierz Mac1, Macierz Mac2);
Macierz operator + (Macierz Mac1, Macierz Mac2);

#endif
