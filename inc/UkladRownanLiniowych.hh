#ifndef UKLADROWNANLINIOWYCH_HH
#define UKLADROWNANLINIOWYCH_HH

/*!
    \file
    \brief Definicja klasy UkladRownanLiniowych

    Plik zawiera definicje klasy Macierz
*/
#include <cmath>
#include <iostream>
#include <Macierz.hh>

/*!
    \brief Modeluje pojęcie ukladu rownan liniowych

    Klasa modeluje pojecie ukladu rownan liniowych. Uklad sklada sie z dwoch czesci:
    -Macierzy glownej zawierajacej wspolczynniki stojace przy niewiadomych
    -Wektora wyrazow wolnych
    Uklad jest reprezentowany w postaci transponowanej.
*/
class UkladRownanLiniowych {
  public:
    /*!
    \brief Zwraca macierz glowna

    Zwraca macierz glowna ukladu rownan.
    */
    Macierz getMacierz() const{ return MacierzGlowna;}

    /*!
    \brief Wstawia macierz glowna

    Funkcja ustawia macierz glowna ukladu rownan.

    \param[in] Mac - Macierz, ktora ma byc wstawiona jako macierz glowna
    */
    void setMacierz(Macierz Mac){ MacierzGlowna = Mac;}

    /*!
    \brief Zwraca wektor wyrazow wolnych

    Zwraca wektor wyrazow wolnych ukladu rownan.
    */
    Wektor getWyrazyWolne() const{ return WyrazyWolne;}

    /*!
    \brief Wstawia wektor wyrazow wolnych

    Funkcja ustawia wektor wyrazow wolnych ukladu rownan.

    \param[in] Wek - wektor, ktory ma byc wstawiony jako wektor wyrazow wolnych
    */
    void setWyrazyWolne(Wektor Wek){ WyrazyWolne = Wek;}

  private:

    /*!
    \brief Macierz glowna

    Macierz zawierajaca wspolczynniki stojace przy niewiadomych rownania.
    */
    Macierz MacierzGlowna;

    /*!
    \brief Wektor wyrazow wolnych

    Wektor zawierajacy wyrazy wolne rownania.
    */
    Wektor WyrazyWolne;
};

/*!
    \brief Rozwiazuje uklad rownan oraz oblicza wektor bledu i jego dlugosc

    Funkcja rozwiazuje uklad rownan oraz oblicza wektor bledu i jego dlugosc a nastepnie wypisuje wszystko na standardowym wyjsciu.

    \param[in] UklRown - uklad rownan, ktory ma byc rozwiazany
*/
void RozwUklad (const UkladRownanLiniowych UklRown);

/*!
    \brief Przeprowadza eliminacje Gaussa

    Funkcja przeprowadza eliminacje Gaussa na podanym ukladzie rownan.

    \param[in] UklRown - uklad rownan, ktory ma byc poddany eliminacji Gaussa

    \return Wektor bedacy rozwiazaniem ukladu rownan
*/
Wektor MetodaGaussa (const UkladRownanLiniowych UklRown);

/*!
    \brief Oblicza wektor bledu

    Funkcja oblicza wektor bledu.

    \param[in] UklRown - uklad rownan dla ktorego obliczny jest wektor bledu
*/
Wektor WekBlad (const UkladRownanLiniowych UklRown);

/*!
    \brief operator >>

    Przeciazenie to sluzy do wprowadzenia ukladu rownan liniowych.

    \param[in] Strm - Adres do strumienia wejsciowego
    \param[in] UklRown - Referencja do UkalduRownanLiniowych, w ktorym maja byc zapisane dane ze strumienia wejsciowego

    \return Adres do strumienia wejsciowego
*/
std::istream& operator >> (std::istream &Strm, UkladRownanLiniowych &UklRown);

/*!
    \brief operator <<

    Przeciazenie to sluzy do wypisania ukladu rownan liniowych.

    \param[in] Strm - Adres do strumienia wyjsciowego
    \param[in] UklRown - Referencja do UkladuRownanLiniowych ktory ma byc wypisany

    \return Adres do strumienia wyjsciowego
*/
std::ostream& operator << ( std::ostream &Strm, const UkladRownanLiniowych &UklRown);


#endif
