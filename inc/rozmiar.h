#ifndef ROZMIAR_H
#define ROZMIAR_H
/*!
    \file
    \brief Definicja ROZMIAR

    ROZMIAR definiuje ilosc niewiadomych w rownaniu a co za tym idzie rozmiar
    macierzy i wektorow.
*/
#define ROZMIAR   10

#endif
