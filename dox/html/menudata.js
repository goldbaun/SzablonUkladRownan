var menudata={children:[
{text:"Strona główna",url:"index.html"},
{text:"Dodatkowe strony",url:"pages.html"},
{text:"Klasy",url:"annotated.html",children:[
{text:"Lista klas",url:"annotated.html"},
{text:"Indeks klas",url:"classes.html"},
{text:"Składowe klas",url:"functions.html",children:[
{text:"Wszystko",url:"functions.html"},
{text:"Funkcje",url:"functions_func.html"},
{text:"Zmienne",url:"functions_vars.html"}]}]},
{text:"Pliki",url:"files.html",children:[
{text:"Lista plików",url:"files.html"},
{text:"Składowe plików",url:"globals.html",children:[
{text:"Wszystko",url:"globals.html",children:[
{text:"d",url:"globals.html#index_d"},
{text:"m",url:"globals.html#index_m"},
{text:"o",url:"globals.html#index_o"},
{text:"r",url:"globals.html#index_r"},
{text:"w",url:"globals.html#index_w"}]},
{text:"Funkcje",url:"globals_func.html",children:[
{text:"d",url:"globals_func.html#index_d"},
{text:"m",url:"globals_func.html#index_m"},
{text:"o",url:"globals_func.html#index_o"},
{text:"r",url:"globals_func.html#index_r"},
{text:"w",url:"globals_func.html#index_w"}]},
{text:"Definicje",url:"globals_defs.html"}]}]}]}
