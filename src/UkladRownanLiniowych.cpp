#include "UkladRownanLiniowych.hh"

using namespace std;

ostream& operator << (ostream& Strm, const UkladRownanLiniowych &UklRown){
    Strm << "Macierz glowna:" << endl;
    Strm << UklRown.getMacierz();
    Strm << "Wektor wyrazow wolnych:" << endl;
    Strm << UklRown.getWyrazyWolne();
    return Strm;
}

istream& operator >> (istream& Strm, UkladRownanLiniowych &UklRown){
    Macierz tmpm;
    Wektor tmpw;
    Strm >> tmpm;
    if(!(Strm.fail()))UklRown.setMacierz(tmpm);
    Strm >> tmpw;
    if(!(Strm.fail()))UklRown.setWyrazyWolne(tmpw);
    return Strm;
}

Wektor MetodaGaussa (const UkladRownanLiniowych UklRown){
    Wektor Wynik;
    Macierz tmpm = UklRown.getMacierz();
    Wektor tmpw = UklRown.getWyrazyWolne();
    double tmpd = 0;
    for (int i = 0; i < (ROZMIAR-1); i++)
    {
        for (int j = (1 + i); j < ROZMIAR; j++)
        {
           tmpd = -(tmpm[i][j]/tmpm[i][i]);
           for (int k = 0; k < ROZMIAR; k++)
           {
               tmpm[k][j] = tmpm[k][j] + (tmpd * tmpm[k][i]);
           }
           tmpw[j] = tmpw[j] + (tmpd * tmpw[i]); 
        }
        
    }

    Wynik[ROZMIAR-1] = tmpw[ROZMIAR-1] / tmpm[ROZMIAR-1][ROZMIAR-1];

    for (int i = (ROZMIAR - 2); i >= 0; i--)
    {
        for (int j = ROZMIAR-1; j > i ; j--)
        {
            Wynik[i] = Wynik[i] - (tmpm[j][j-(j-i)] * Wynik[j]);   
        }
        Wynik[i] =(tmpw[i] + Wynik[i]) / tmpm[i][i];
    }
    
    return Wynik;
}

Wektor WekBlad (const UkladRownanLiniowych UklRown){
    Wektor Wynik;
    Wektor Rozwiazanie = MetodaGaussa(UklRown);
    return (Wynik = (UklRown.getMacierz() * Rozwiazanie) - UklRown.getWyrazyWolne());
}

double DlugoWekBlad (const Wektor Wek){
    double Wynik;
    return (Wynik = sqrt(pow(Wek * Wek, 2)));
}

void RozwUklad (const UkladRownanLiniowych UklRown){
    Wektor Wynik = MetodaGaussa(UklRown);
    Wektor Blad = WekBlad(UklRown);
    double DlugoBlad = DlugoWekBlad(Blad);

    if(isnan(Wynik[0])) cout << "Uklad jest nieoznaczony" << endl;
    else
    {
        if(isinf(Wynik[0])) cout << "Uklad jest sprzeczny" << endl;
        else
        {
            cout << "Rozwiazanie ukladu to: ";
            cout << Wynik << endl;
            cout << "Wektor bledu to: ";
            cout << Blad << endl;
            cout << "Dlugosc wektora bledu to: ";
            cout << DlugoBlad << endl;
        }
    }
}

