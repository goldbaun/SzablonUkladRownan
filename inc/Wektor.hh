#ifndef WEKTOR_HH
#define WEKTOR_HH

/*!
    \file
    \brief Definicja klasy Wektor

    Plik zawiera definicje klasy Wektor
*/
#include "rozmiar.h"
#include <iostream>


/*!
    \brief Modeluje pojęcie wektora

    Klasa modeluje pojecie wektora. Rozmiar wektora definiuje stała ROZMIAR.
    Wektor jest reprezentowany jako tablica.
*/
class Wektor {
  public:

  /*!
    \brief Inicjalizuje wektor

    Inicjalizuje wektor poprzez wypełnienie tablicy zerami.
  */
  Wektor(){for (int i = 0; i < ROZMIAR; i++)
  {
    Zmienna[i] = 0;
  }
  }

  /*!
    \brief operator []

    Zwraca wartosc z podanej pozycji z wiersza.

    \param[in] index - indeks zmiennej w wierszu

    \return Zwraca wartosc z podanej pozycji wiersza
  */
  double operator [](unsigned int index) const{
    return Zmienna[index];
  }

  /*!
    \brief operator []

    Wstawia wartosc na podana pozycje wiersza.

    \param[in] index - indeks zmiennej w wierszu

    \return Adres do podanej pozycji wiersza
  */
  double& operator [](unsigned int index){
    return Zmienna[index];
  }

  private:

  /*!
    \brief Wartosci w wierszu wektora

    Tablica zawierajaca wartoscic w wierszu wektora.
  */
  double Zmienna[ROZMIAR];
};


/*!
    \brief operator >>

    Przeciazenie to sluzy do wprowadzenia wektora.

    \param[in] Strm - Adres do strumienia wejsciowego
    \param[in] Wek - Referencja do Wektora, w ktorym maja byc zapisane dane ze strumienia wejsciowego

    \return Adres do strumienia wejsciowego
*/
std::istream& operator >> (std::istream &Strm, Wektor &Wek);

/*!
    \brief operator <<

    Przeciazenie to sluzy do wypisania wektora.

    \param[in] Strm - Adres do strumienia wyjsciowego
    \param[in] Wek - Referencja do Wektora ktory ma byc wypisany

    \return Adres do strumienia wyjsciowego
*/
std::ostream& operator << (std::ostream &Strm, const Wektor &Wek);

/*!
    \brief operator +

    Przeciazenie to sluzy do dodawania wektorow.

    \param[in] Wek1 - Pierwszy skladnik dodawania
    \param[in] Wek2 - Drugi skladnik dodawania

    \return Wektor bedacy wynik dodawania
*/
Wektor operator + (Wektor Wek1, Wektor Wek2);

/*!
    \brief operator -

    Przeciazenie to sluzy do odejmowania wektorow.

    \param[in] Wek1 - Pierwszy skladnik odejmowania
    \param[in] Wek2 - Drugi skladnik odejmowania

    \return Wektor bedacy wynik odejmowania
*/
Wektor operator - (Wektor Wek1, Wektor Wek2);

/*!
    \brief operator *

    Przeciazenie to sluzy do mnozenia wektora przez liczbe rzeczywista.

    \param[in] Wek - Pierwszy skladnik mnozenia (Wektor)
    \param[in] Skl - Drugi skladnik mnozenia (Double)

    \return Wektor bedacy wynik mnozenia
*/
Wektor operator * (Wektor Wek, double Skl);

/*!
    \brief operator *

    Przeciazenie to sluzy do obliczenia iloczynu skalarnego.

    \param[in] Wek1 - Pierwszy skladnik
    \param[in] Wek2 - Drugi skladnik

    \return Liczba bedaca wynikiem iloczynu skalarnego
*/
double operator * (Wektor Wek1, Wektor Wek2);

/*!
    \brief operator /

    Przeciazenie to sluzy do dzielenia wektora przez liczbe rzeczywista.

    \param[in] Wek - Pierwszy skladnik dzielenia (Wektor)
    \param[in] Skl - Drugi skladnik dzielenia (Double)

    \return Wektor bedacy wynik dzielenia
*/
Wektor operator / (Wektor Wek, double Skl);

#endif
